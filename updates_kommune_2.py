import pandas as pd

knr_reference = pd.read_csv("data/20220222_Kommuner over tid.txt", sep="\t", dtype="string")
knr_reference["knr_suffix"] = knr_reference.apply(lambda x: list(knr_reference[knr_reference.KNR == x["KNR"]].sort_values("Opprettet")["Opprettet"]).index(x["Opprettet"]), axis = 1)
knr_reference["knr_suffix"] = knr_reference["knr_suffix"].apply(lambda x: chr(97+x))

knr_reference.to_csv("data/2022-03-11_Knr_med_suffiks.csv", sep="\t", index = False)