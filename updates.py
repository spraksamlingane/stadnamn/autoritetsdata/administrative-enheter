import pandas as pd


# Kommuner 
knr_reference = pd.read_csv("data/kommuner_over_tid_master.txt", sep="\t", dtype="string")
knr_reference["knr_suffix"] = knr_reference.apply(lambda x: list(knr_reference[knr_reference.KNR == x["KNR"]].sort_values("Opprettet")["Opprettet"]).index(x["Opprettet"]), axis = 1)
knr_reference["knr_suffix"] = knr_reference["knr_suffix"].apply(lambda x: chr(97+x))

knr_reference.to_csv("data/knr_refined.csv", sep="\t", index = False)




# Fylker
fnr_reference = pd.read_csv("data/Fylker over tid.txt", sep="\t", dtype="string")
fnr_reference["fnr_suffix"] = fnr_reference.apply(lambda x: list(fnr_reference[fnr_reference.FNR == x["FNR"]].sort_values("fra")["fra"]).index(x["fra"]), axis = 1)
fnr_reference["fnr_suffix"] = fnr_reference["fnr_suffix"].apply(lambda x: chr(97+x))

fnr_reference["Fylkenavn"] = fnr_reference.Fylkenavn.str.strip()
fnr_reference.to_csv("data/fnr_refined.csv", sep="\t", index = False)