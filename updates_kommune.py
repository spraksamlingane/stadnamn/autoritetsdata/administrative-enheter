import pandas as pd
import csv

data = pd.read_csv("data/20220222_Kommuner over tid.txt", sep="\t", dtype=str, index_col=0)
data["KNR_suffix"] = "1"
data["prefLabel"] = data.Kommune

with open('updates/duplikater_knr.csv') as csvfile:
    reader = csv.reader(csvfile, delimiter='\t')
    next(reader)
    for row in reader:
        row_id, suffix, prefLabel = row[0], row[-2], row[-1]
        data.loc[int(row_id), "KNR_suffix"] = suffix
        if prefLabel:
            data.loc[int(row_id), "prefLabel"] = prefLabel

# List of knr that require manual ID-assignment (KNR + suffix) in dataset
data[data.KNR_suffix > "1"][["KNR"]].drop_duplicates().to_csv("data/knr_med_duplikater.txt", sep="\t", index = False)

data.to_csv("data/kommuner_over_tid_2022-02-22_b.txt", sep="\t")