from rdflib import Namespace, Literal, Graph, RDFS
import uuid
import csv

SV = Namespace("https://data.toponymi.spraksamlingane.no/id")
DCT = Namespace("http://purl.org/dc/terms/")
get_uuid = lambda x: str(uuid.uuid3(uuid.NAMESPACE_URL, "http://data.toponym.ub.uib.no/graph/"+x))

graph = Graph()

with open('data/2022-03-11_Knr_med_suffiks.csv', encoding="utf-8") as csvfile:
    reader = csv.reader(csvfile, delimiter='\t')
    next(reader)
    for row in reader:
        knr = row[4]
        knr_suffix = row[7]
        label = row[2]
        seed = f"KNR_{knr}{knr_suffix}"
        uri = SV[get_uuid(seed)]
        graph.add((uri, DCT.identifier, Literal(knr)))
        graph.add((uri, RDFS.label, Literal(label)))


with open('data/Fylker over tid.txt', encoding="utf-8") as csvfile:
    reader = csv.reader(csvfile, delimiter='\t')
    next(reader)
    for row in reader:
        row = [x.strip().replace("\xa0", " ") for x in row]
        fnr = row[0]
        label = row[1]
        begin = row[4]
        minority_languages = row[6:10]
        uri = SV[get_uuid("FNR_"+fnr+"_"+str(begin))]
  
        for name, code in zip(minority_languages, ["fkv", "sme", "sma", "smj"]):
            if name != "":
                graph.add((uri, RDFS.label, Literal(name, lang= code)))
        graph.add((uri, DCT.identifier, Literal(knr)))
        graph.add((uri, RDFS.label, Literal(label, lang="nor")))


graph.serialize("adm.nt", format="nt")